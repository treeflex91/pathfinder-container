## Intro
You can use Traefik to front your pathfinder server easily. Traefik
understands docker natively and will let you easily configure multiple
backend services using docker tags. Traefik also supports Let's Encrypt and
makes it very easy to get SSL certificates issued for your domain.

By default the `docker-compose.yml` in this folder will create a Traefik
instance configured to find and expose the Pathfinder container defined in
`../docker-compose.yml`. You can check the labels on the container
definition in that file to learn a little more about how this works.

## Production
1. If you're happy with your settings and are ready for production, remove
the beta CA server line from `traefik/docker-compose.yml`. You will also need
to delete the configuration file so Let's Encrypt will get a new certificate. The
file can be found on the container mount at: `traefik/letsencrypt/acme.json`.

Don't make this change until you've got everything working and are happy! Let's
Encrypt has strict rate limits and if you exceed your certificate request rate for
a domain name you will have to wait a week before a new certificate can be issued
to you.